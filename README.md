# Seminarni prace

## Zadání Seminární práce
Mám sadu molekul v chemickém lineárním zápisu SMILES, tento chemický lineární zápis budu převádět do dalších chemických lineárních zápisů (DeepSMILES, SLN, SELFIES, InCHI). Tyto chemické lineární zápisy budu tokenizovat pomocí metody BPE (Byte Pair Encoding) do vektoru, který pak vložím do neuronové sítě LSTM, která bude predikovat biologickou aktivitu v podobě pIC50. Výslednou hodnotu biologické aktivity pro jednotlivé chemické lineární zápisy budu mezi sebou porovnávat, abych zjistila, který chemický lineární zápis je vhodný použít pro LSTM síť po BPE tokenizaci.

## Soubory v příslušném adresáři
data:
- VEGF2_ChEMBL28-10980_pic50_all_chem_linear_notaion.csv    
        - csv soubor, 
        
        - načten v souboru MVI_tokenization.ipnyb pro tokenizaci
        
        - obsahuje molekulu zapsanou ve SMILES zápisu, k ní příslušnou hodnotu biologické aktivity (sloupeček value - pIC50) a další chemické lineární zápisy (sln, deep_smiles, selfies, inchi)
        
        - na začátek a konec všech zápisů jsem přidala znaky, které označují okraj (začátek - ^, konec - $)
- VEGF2_ChEMBL28-10980_pic50_BPE_list_tokens_ids.csv
        - csv soubor
        
        - vytvořen po BPE tokenizaci
        
        - obsahuje k původním chemickým lineárním zápisům a biologické aktivitě encoding pro jednotlivé chemické lineární zápisy a vektory s id pro tokeny, které vyhodil BPE 

        models/check:
                - obsahuje uložené modely pro jednotlivé chemické lineární zápisy a historii

jupyter:
- MVI_tokenization.ipnyb   
        - jupyter notebook skript
        
        - provádí se BPE tokenizace na všech linárních zápisech (SMILES, DeepSMILES, SELFIES, SLN, InCHI)
        
        - výsledek tokenizace (enkódování + vektory s id tokeny) je uložen v souboru VEGF2_ChEMBL28-10980_pic50_BPE_list_tokens_ids.csv
- MVI_prediction_biological_activity_ANN.ipnyb
        - jupyter notebook skript
        
        - zde je implementovaný kód na neuronovou síť LSTM predikujicí bioligickou aktivitu
- number_of_token_ids.png
        - obrázek histogramů pro délky listů s token ids pro jednotlivé chemické lineární zápisy
        
## Spuštění
- MVI_tokenization.ipnyb    
        - v linuxu spustit příkaz jupyter notebook a pak v jupyter notebooku otevřít a postupně spouštět kód v buňkách (cells) 
- MVI_prediction_biological_activity_ANN.ipnyb
        - v linuxu spustit příkaz jupyter notebook a pak v jupyter notebooku otevřít a postupně spouštět kód v buňkách (cells)                           